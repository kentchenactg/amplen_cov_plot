#! /usr/bin/env python3
# coding: utf-8
# Author: KentChen

##########################
# Plot the "amplicon length v.s. coverage" for each pool of panel within a run
##########################

import os, sys, subprocess, json, argparse, logging, glob, io, math
import pandas as pd
import numpy as np
from pathlib import Path
import matplotlib.pyplot as plt
# from matplotlib.pyplot import cm
import warnings
from pandas.core.common import SettingWithCopyWarning
warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)

ver = 'v1.0.0'

##### json loading #####
def load_general_json():
    with open(args.general_cfg, 'r') as file2:
        general_json = json.load(file2)
    return general_json
########################

##### General Functions #####
def read_tsv(anno_tsv, header_YN):
    if (header_YN == 'Y'):
        df = pd.read_table(anno_tsv, sep='\t', skip_blank_lines=True, dtype='unicode')
        pd.set_option('display.max_colwidth', None)
        return df
    elif (header_YN == 'N'):
        df = pd.read_table(anno_tsv, sep='\t', skip_blank_lines=True, dtype='unicode', header=None)
        pd.set_option('display.max_colwidth', None)
        return df
########################

##### Main Functions #####
def concat_qc_files_and_create_folders():
    runBCL_path = f"{lv1_path}/{sequencer_ID}/BCL/{run_ID}/bcl2fastq_report"

    if not os.path.exists(lv1_path):
        logging.error(f"### ERROR: {lv1_path} not found !")
        sys.exit()
    
    if not os.path.exists(runBCL_path):
        logging.error(f"### ERROR: {runBCL_path} not found !")
        sys.exit()
    
    if not any(glob.iglob(f"{runBCL_path}/*.sampleQC.xls")):
        logging.error(f"### ERROR: no any 'sampleQC.xls' found !")
        sys.exit()
    
    # 讀取BCL資料夾中所有含有字尾是"*.sampleQC.xls"的檔案來合併
    df_merged = pd.DataFrame()
    qc_files = glob.glob(f"{runBCL_path}/*.sampleQC.xls")
    for qc_file in qc_files:
        df = read_tsv(qc_file, header_YN='Y')
        df_merged = pd.concat([df_merged, df], ignore_index=True)
    
    # 依據panel_ID欄位來產生資料夾
    for panel_ID in df_merged['Panel_ID'].unique():
        Path(f"{output_path}/{panel_ID}").mkdir(parents=True, exist_ok=True)
    
    if not os.path.exists(output_path):
        logging.error(f"### ERROR: {output_path} not found! Make sure there is any QC data under {runBCL_path}")
        sys.exit()
    
    return df_merged

def generate_amplicon_length_and_cov_tables():
    # 處理每個samples，透過bedtools coverage & intersect取得amplicon length & coverage資訊
    for runBC_sampleID_panelID in runBC_sampleID_panelIDs:
        runBC_sampleID = runBC_sampleID_panelID.split(";")[0] + '_' + runBC_sampleID_panelID.split(";")[1]
        
        Sample_panel_ID = runBC_sampleID_panelID.split(";")[2]
        panel_amplicon_bed = df_panel_conf[df_panel_conf['Panel_ID'].str.contains(Sample_panel_ID)]['Amplicon_Bed'].to_string(index=False)
        panel_ampInsert_bed = df_panel_conf[df_panel_conf['Panel_ID'].str.contains(Sample_panel_ID)]['AmpInsert_Bed'].to_string(index=False)
        
        processed_bams = glob.glob(f"{lv1_path}/{sequencer_ID}/Processed_BAM/{run_ID}/{runBC_sampleID}*.processed.bam")
        processed_bams_str = ''.join(processed_bams)
        
        ## [注意] 我是拿 4cols 的 amplicon bed；以及 6cols 的 ampInsert bed (包含pool資訊)，若bed格式改變，這邊也會需要更改cut --complement的欄位
        if not any(glob.iglob(f"{output_path}/{Sample_panel_ID}/{runBC_sampleID}.processed.amplicon.cov.xls")):
            
            ### 這邊使用的是bedtools coverage先處理amplicon bed，再用bedtools intersect來和ampInsert bed取交集，並剔除不必要的欄位
            bed_cmd = subprocess.getoutput(f"{bedtools} coverage -a {panel_amplicon_bed} -b {processed_bams_str} |\
                                   {bedtools} intersect -a {panel_ampInsert_bed} -b - -f 1 -wa -wb | sort -V -k4,4 -u |\
                                    cut --complement -f5,12,14")
            
            header = ["processed_chr", "processed_start", "processed_end", "processed_ampID", "processed_ampInfo", "raw_chr", "raw_start", "raw_end", "raw_ampID", "raw_ampCov", "raw_ampLength"]
            df1 = pd.read_table(io.StringIO(bed_cmd), sep='\t', skip_blank_lines=True, dtype='unicode', names=header)
            
            ### 將Pool & Panel_ID information獨立成兩欄
            df1['Pool'] = np.where(df1['processed_ampInfo'].str.contains(';'), df1['processed_ampInfo'].str.split(';').str[2].str.split('=').str[1], df1['processed_ampInfo'])
            df1['RunBC_sampleID'] = runBC_sampleID
            df1['Panel_ID'] = Sample_panel_ID
            df1.to_csv(f"{output_path}/{Sample_panel_ID}/{runBC_sampleID}.processed.amplicon.cov.xls", sep='\t', index=False)
            
            logging.info(f"--- Generate {runBC_sampleID}.processed.amplicon.cov.xls complete ---")


def extract_dataframe_by_pools():
    # 將前一段function產生的amplicon.cov.xls，全部合併然後餵給下一個function
    df_all = pd.DataFrame()
    for runBC_sampleID_panelID in runBC_sampleID_panelIDs:
        runBC_sampleID = runBC_sampleID_panelID.split(";")[0] + '_' + runBC_sampleID_panelID.split(";")[1]
        Sample_panel_ID = runBC_sampleID_panelID.split(";")[2]
    
        bed_tsvs = glob.glob(f"{output_path}/{Sample_panel_ID}/{runBC_sampleID}.processed.amplicon.cov.xls")
        bed_tsv = ''.join(bed_tsvs)
        df = read_tsv(bed_tsv, header_YN='Y')
        df_all = pd.concat([df_all, df], ignore_index=True)
    
    return df_all

    
def plot_pools_by_panel(df_all):
    ############### Plot for ACTRisk panel ###############
    df_all['General_panel_ID'] = df_all['Panel_ID'].str[:5] # (e.g. PA031ANA ---> PA031) 目的是同一個大panel要來計算max coverage和max length
    
    # 批次處理每個panels
    for General_panel_ID in df_all['General_panel_ID'].unique().tolist():

        ## 同一個大panel要來計算amplicon length和coverage的最大/最小值 (只要是相同的panel繪製的圖，所有樣本的x & y axis都是固定的)
        df_all_pools = df_all[df_all['Panel_ID'].str.contains(General_panel_ID)]
        min_cov, max_cov, min_ampLen, max_ampLen = calc_panel_ampLen_and_cov(df_all_pools, General_panel_ID)

        ## 針對不同pool進行繪製並給定顏色 (ACThrd: pool 1/2、ACTHRD: pool 1/2/3)
        colors = ['blue', 'green', 'yellow', 'red', 'grey']
        for idx in range(len(colors)):
            num_of_pool = idx + 1
            df_all_pools.loc[(df_all_pools['Pool'] == f'{num_of_pool}'), 'Color'] = colors[idx]


        ## 批次處理每個samples
        for runBC_sampleID_panelID in runBC_sampleID_panelIDs:
            runBC_sampleID = runBC_sampleID_panelID.split(";")[0] + '_' + runBC_sampleID_panelID.split(";")[1]
            Sample_panel_ID = runBC_sampleID_panelID.split(";")[2]
            
            panel_md_exon = df_panel_conf[df_panel_conf['Panel_ID'].str.contains(Sample_panel_ID)]['Mean_depth_exon_snp'].to_string(index=False).split(',')[0]
            panel_md_snp = df_panel_conf[df_panel_conf['Panel_ID'].str.contains(Sample_panel_ID)]['Mean_depth_exon_snp'].to_string(index=False).split(',')[1]
            panel_num_of_pool = pd.to_numeric(df_panel_conf[df_panel_conf['Panel_ID'].str.contains(Sample_panel_ID)]['Pools'].to_string(index=False))

            if General_panel_ID in Sample_panel_ID:
                df_all_pools_per_sample = df_all_pools[df_all_pools['RunBC_sampleID'].str.contains(runBC_sampleID)]
                
                ### 以ACTRisk來說，雖然pool=2，但由於還需要多畫pool 1+2，所以需要多一張圖，總共三張
                fig, ax = plt.subplots(1, (pd.to_numeric(panel_num_of_pool) + 1), figsize=(16,9), dpi=100)
                fig.suptitle(f"Amplicon Length v.s. Coverage - {Sample_panel_ID}\n({runBC_sampleID})", size=16)
                
                ### 批次處理每個pool，依據不同的Panel繪製不同數量的圖 (ACTRisk: pool 1,2、ACTHRD: pool 1,2,3)
                merged_name_of_pools = []
                # colors = cm.rainbow(np.linspace(0, 1, panel_num_of_pool))
                # for num_of_pool_idx, color in zip(range(panel_num_of_pool), colors):
                for num_of_pool_idx in range(panel_num_of_pool):
                    num_of_pool_idx = pd.to_numeric(num_of_pool_idx)
                    num_of_pool = num_of_pool_idx + 1
                    df_each_pool_per_sample = df_all_pools_per_sample[df_all_pools_per_sample['Pool'].str.contains(f'{num_of_pool}')]
                    df_each_pool_per_sample.loc[(df_each_pool_per_sample['Pool'] == num_of_pool), 'Color'] = colors[num_of_pool_idx]
                    
                    #### 實際scatter plot的function在下方 ####
                    scatter_plot(df_all_pools_per_sample, min_cov, max_cov, min_ampLen, max_ampLen, df_each_pool_per_sample, ax, num_of_pool_idx, f"Pool {num_of_pool}", panel_md_exon, panel_md_snp)

                    merged_name_of_pools.append(num_of_pool)

                ### 全部pools集合起來繪製重疊圖 (ACTRisk: pool 1+2、ACTHRD: pool 1+2+3)
                merged_name_of_pools_str = '+'.join(map(str, merged_name_of_pools))
                scatter_plot(df_all_pools_per_sample, min_cov, max_cov, min_ampLen, max_ampLen, df_all_pools_per_sample, ax, panel_num_of_pool, f"Pool {merged_name_of_pools_str}", panel_md_exon, panel_md_snp)
                
                plt.savefig(f"{output_path}/{Sample_panel_ID}/{runBC_sampleID}.processed.amplicon.cov.png")
                plt.close()

                logging.info(f"--- Plot {runBC_sampleID}.processed.amplicon.cov.png ({Sample_panel_ID}) ---")


def calc_panel_ampLen_and_cov(df, main_panel_ID):
    min_cov = round(pd.to_numeric(df['raw_ampCov']).min(), 0)
    max_cov = round(pd.to_numeric(df['raw_ampCov']).max(), 0)
    min_ampLen = round(pd.to_numeric(df['raw_ampLength']).min(), 0)
    max_ampLen = round(pd.to_numeric(df['raw_ampLength']).max(), 0)
    
    logging.info(f""" Total {len(df['RunBC_sampleID'].unique().tolist())} samples in {main_panel_ID} panel
    * Minimum Amplicon Length: {min_ampLen}
    * Maximum Amplicon Length: {max_ampLen}
    * Minimum Amplicon Coverage: {min_cov}
    * Maximum Amplicon Coverage: {max_cov}
    """)
    
    return min_cov,max_cov,min_ampLen,max_ampLen

def scatter_plot(df, min_cov, max_cov, min_ampLen, max_ampLen, df_pool_x, ax, i, name_of_pool, exon_cutoff, snp_cutoff):
    ax[i].scatter(x=pd.to_numeric(df_pool_x['raw_ampLength']), y=round(pd.to_numeric(df_pool_x['raw_ampCov']), 0), alpha=0.3, c=df_pool_x['Color'], s=5)
    
    # 自動取得interval (湊整數)：
    # 1.) 先取得max-min後數值的位數 ； 2.) 將數值除以10的(位數次方-1) ； 3.) 將數值無條件進位 ； 4.) 將數值乘以10的(位數次方-1)
    # (e.g. (275-105)/10=17為二位數 ； 17/10^1=1.7 ； 1.7無條件進位=2 ； 2*10^1=20)
    num_of_digit = len(str(int((max_ampLen - min_ampLen) / 10)))
    ampLen_interval = math.ceil((((max_ampLen - min_ampLen) / 10) / (10 * (num_of_digit - 1)))) * (10 * (num_of_digit - 1))
    num_of_digit = len(str(int((max_cov - min_cov) / 20)))
    cov_interval = math.ceil((((max_cov - min_cov) / 20) / (10 * (num_of_digit - 1)))) * (10 * (num_of_digit - 1))
    
    ax[i].set_xticks(np.arange(min_ampLen, max_ampLen, ampLen_interval))
    ax[i].set_yticks(np.arange(min_cov, max_cov, cov_interval))
    ax[i].set_xlim(min_ampLen - ampLen_interval, max_ampLen + ampLen_interval)
    ax[i].set_ylim(min_cov - cov_interval, max_cov + cov_interval)
    ax[i].set_title(name_of_pool)

    # 繪製amplicon coverage (y-axis) 的PR99水平線 (可幫助觀察outlier)
    PR99_cov = round(pd.to_numeric(df['raw_ampCov']).quantile(0.99), 0)
    if snp_cutoff == 'NaN':
        ypos = [PR99_cov, int(exon_cutoff)]
    else:
        ypos = [PR99_cov, int(exon_cutoff), int(snp_cutoff)]
    perc = ['99PR', 'exon cutoff', 'snp cutoff']
    color = ['r', 'y', 'y']
    cnt = 0
    for yc in ypos:
        ax[i].axhline(y=yc, color=f'{color[cnt]}', linestyle='--', label=f'{ypos[cnt]} ({perc[cnt]})', alpha=0.3)
        # ax[i].text(x=0, y=yc, s=yc)
        cnt += 1
    ax[i].legend(loc='upper right')


########################



##### main workflow #####
if __name__ == "__main__":
    cwd = os.getcwd()

    parser = argparse.ArgumentParser(description='Run Sample Comparison Plot')
    parser.add_argument('-g', '--general_cfg', help='存放基本訊息的json (configs/general_config.json)', default="configs/general_config.json")
    parser.add_argument('-p', '--panel_cfg', help='存放panel訊息的tsv (configs/panel_configurations.tsv)', default="configs/panel_configurations.tsv")
    args = parser.parse_args()

    ### block of generate logging module configuration 
    #########################################
    log_file = 'ampLen_cov_plot.log'
    logging.basicConfig(level=logging.INFO,
        format='%(asctime)s %(levelname)s %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S',
        handlers=[
            logging.FileHandler(log_file),
            logging.StreamHandler()
        ]
    )
    #########################################

    ### block of variable loaded from json
    #########################################
    general_json = load_general_json()

    output_path = general_json['output_path']
    lv1_path = general_json['lv1_path']
    sequencer_ID = general_json['sequencer_ID']
    run_ID = general_json['run_ID']
    bedtools = general_json['Tools']['bedtools']
    #########################################



    ### main pipeline
    #########################################
    df_panel_conf = read_tsv(args.panel_cfg, header_YN="Y")

    logging.info(f"### Run Amplicon Length v.s. Coverage Plot by Pools of Panel in a Run... ###\n")
    logging.info(f"### VERSION SRC: ampLen_cov_plot_pipeline_{ver}")
    logging.info(f"""
*****************************************
* Current Working Dir ---> {cwd} 
* Configuration ---> 'General config': {args.general_cfg}, 'Panel config': {args.panel_cfg}
* Log file ---> {log_file}
*****************************************
""")

    print("")


    logging.info(f"*** Concat QC files and Create Folders... \n")
    concat_qc_files_and_create_folders()

    logging.info(f"*** Generate amplicon length and coverage tables... \n")
    df = concat_qc_files_and_create_folders()
    runBC_sampleID_panelIDs = df['Runbarcode'] + ';' + df['Sample_ID'] + ';' + df['Panel_ID']

    generate_amplicon_length_and_cov_tables()

    logging.info(f"*** Extract dataframe by pools... \n")
    df_all = extract_dataframe_by_pools()

    logging.info(f"*** Plot pools of [amplicon Length v.s. Coverage] figure by panel... \n")
    plot_pools_by_panel(df_all)


    print("")
    #########################################

    logging.info("### PIPELINE COMPLETED ! ###")